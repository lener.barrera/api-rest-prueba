const { QueryTypes } = require("sequelize");

const models = require("./models/index");
const { sequelize } = models;

executeprocedure = async (query, replacement) => {
    return await sequelize.query(query, {
        replacements: replacement,
        type: QueryTypes.SELECT
    }).then((res) => {
        return {
            data: res[0],
            status: 200,
            error: null
        };
    }).catch((err) => {
        return {
            data: null,
            status: 500,
            error: err
        };
    });
}

module.exports = { executeprocedure };