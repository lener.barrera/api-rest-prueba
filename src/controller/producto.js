const { validationResult } = require("express-validator");

const { executeprocedure } = require("../database/genericrepository");

agregarProducto = async (req, res) => {
    const { Descripcion, CodigoProducto, Precio } = req.body;

    const result = await executeprocedure("EXEC dbo.spProductoAgregar @Descripcion = :Descripcion, @CodigoProducto = :CodigoProducto, @Precio = :Precio", { Descripcion, CodigoProducto, Precio });

    return res.status(result.status).json(result);
}

actualizarProducto = async (req, res) => {
    const { Id } = req.params;
    const { Descripcion, CodigoProducto, Precio, Activo } = req.body;

    const result = await executeprocedure("EXECUTE dbo.spProductoActualizar @ProductoId = :ProductoId, @Descripcion = :Descripcion, @CodigoProducto = :CodigoProducto, @Precio = :Precio, @Activo = :Activo", { ProductoId: Id, Descripcion, CodigoProducto, Precio, Activo: (Activo ? Activo : 1) });

    return res.status(result.status).json(result);
}

eliminarProducto = async (req, res) => {
    const { Id } = req.params;

    const result = await executeprocedure("EXEC dbo.spProductoEliminar @ProductoId = :ProductoId", { ProductoId: Id });

    return res.status(result.status).json(result);
}

detalleProducto = async (req, res) => {
    const validaResult = validationResult(req);
    if (validaResult.isEmpty()) {
        return res.status(400).json({
            Mensaje: `El Id no puede ser null, ${req.query.Id}!`,
            status: 400
        });
    }

    const { Id } = req.params;

    const result = await executeprocedure("EXECUTE dbo.spProductoObtener @ProductoId = :ProductoId", { ProductoId: Id });

    return res.status(result.status).json(result);
}

descuentoProductoPorMes = async (req, res) => {
    const { Mes } = req.params;

    const result = await executeprocedure("EXEC dbo.spProductoDescuetoxMes @Mes = :Mes", { Mes });

    return res.status(result.status).json(result);
}

module.exports = {
    detalleProducto,
    agregarProducto,
    actualizarProducto,
    eliminarProducto,
    descuentoProductoPorMes
}