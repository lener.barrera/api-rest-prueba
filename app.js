const express = require("express");
const { query } = require("express-validator");
const { actualizarProducto, detalleProducto, agregarProducto, eliminarProducto, descuentoProductoPorMes } = require("./src/controller/producto");

const app = express();

app.use(express.json());
app.use(express.urlencoded());

app.get("/host/api/producto/:Id", query("Id").notEmpty(), detalleProducto);
app.post("/host/api/producto", agregarProducto);
app.put("/host/api/producto/:Id", actualizarProducto);
app.delete("/host/api/producto/:Id", eliminarProducto);
app.get("/host/api/productos/:Mes", descuentoProductoPorMes);

module.exports = app;